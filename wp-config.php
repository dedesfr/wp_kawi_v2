<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_kawi_v2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'dede');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ieWuy,H}z#4XI>voPS!NmW3Kek`Oax^Qzu5wp}^m?>SW$C+3@W@*?Ak.54qAdg8<');
define('SECURE_AUTH_KEY',  '_J_/#~eeT NXZPl_UfGNK^GA0f9@GH|!7RU;jJ:jLkY$O:Dz;?*%U+@}H~kr_xh|');
define('LOGGED_IN_KEY',    'P gDLzu^!a_,4]fxcQSy&]z~bs%50}rc(i.CuFt;~JT(5$}%,%wHNp|<?Bf V_AZ');
define('NONCE_KEY',        'H`EkIP8c6iMQi^K0A!PspE]7L(Vxld7mAN#<#v-eJ]%rC4Mf Ag,181r2g7ABTpW');
define('AUTH_SALT',        '7,NI`2e1^WxclN3Uq 0}R`G*RoG6.lSBC1Rl8-Era?QC{iks?#wcH70#rLKELz^k');
define('SECURE_AUTH_SALT', ':6P;%j_)=O=zC?u5-~my.gM ]HciYb4su/$0x@Iy-gG8W/YzS4tz#XuLtiP`Sho-');
define('LOGGED_IN_SALT',   'z?7}OOttMsO/zy+xgbHekyMN!aZtAnUg5^- 0!|j0P9F?aWvZAAIFC{8K2`B76?w');
define('NONCE_SALT',       'CL$dp+tFiL/kuck#VDM ;;~7$4`s9t@VBA7VwNE-54C+pnU=kl>!OHr?Pt3&I/ii');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
