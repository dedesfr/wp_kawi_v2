<?php

/**
 * Include CSS files 
 */
function theme_enqueue_scripts() {
        wp_enqueue_style( 'Font_Awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
        wp_enqueue_style( 'Bootstrap_css', get_template_directory_uri() . '/css/bootstrap.css' );
        wp_enqueue_style( 'MDB', get_template_directory_uri() . '/css/mdb.css' );
        wp_enqueue_style( 'Style', get_template_directory_uri() . '/style.css' );
        wp_enqueue_script( 'dede-js', get_template_directory_uri()."/style.js", array('jQuery'), null, true );
        wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/js/jquery-2.2.3.min.js', array(), '2.2.3', true );
        wp_enqueue_script( 'Tether', get_template_directory_uri() . '/js/tether.min.js', array(), '1.0.0', true );
        wp_enqueue_script( 'Bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true );
        wp_enqueue_script( 'MDB', get_template_directory_uri() . '/js/mdb.js', array(), '1.0.0', true );
        wp_enqueue_script( 'Stellar', get_template_directory_uri() . '/stellar/jquery.stellar.js', array('jQuery'), null, true );
        wp_enqueue_script( 'Waypoint', get_template_directory_uri() . '/waypoint/jquery.waypoints.min.js', array('jQuery'), null, true );
        }
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );


	/**
	 * Include external files
	 */
	require_once('inc/mdb_bootstrap_navwalker.php');
	             
	/**
	 * Setup Theme
	 */
	function MDB_setup() {
	  register_nav_menus(array(
	    'navbar' => __( 'Navbar Menu'),
	    'footer1' => __( 'Footer #1 Column'),
	    'footer2' => __( 'Footer #2 Column'),
	    'footer3' => __( 'Footer #3 Column')
	    ));
	  // Add featured image support
	    add_theme_support('post-thumbnails');
	    add_image_size('main-full', 1078, 516, false); // main post image in full width
	  }
	  add_action('after_setup_theme', 'MDB_setup');

	/**
	 * Register our sidebars and widgetized areas.
	 */
	function mdb_widgets_init() {

	  register_sidebar( array(
	    'name'          => 'Sidebar',
	    'id'            => 'sidebar',
	    'before_widget' => '',
	    'after_widget'  => '',
	    'before_title'  => '',
	    'after_title'   => '',
	  ) );
	  register_sidebar( array(
	    'name'          => 'Footer widget area',
	    'id'            => 'footer',
	    'before_widget' => '',
	    'after_widget'  => '',
	    'before_title'  => '',
	    'after_title'   => '',
	  ) );

	}
	add_action( 'widgets_init', 'mdb_widgets_init' );

	/**
	 * Include external files
	 */
	require_once('inc/mdb_bootstrap_navwalker.php');
	require_once('inc/mdb_pagination.php'); 
?>
