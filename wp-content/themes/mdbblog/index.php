<?php get_header(); ?>
<main>
	<!--Main layout-->
	<div class="para_head" data-stellar-background-ratio="0.5"></div>
	<div class="container">
		<div id="content">
			<div class="row">
				<!--Main column-->
				<div class="col-md-8" id="coba">
					<?php
	            if ( have_posts() ) {
	            while ( have_posts() ) {
	            the_post();
	            ?>
					<!--Post-->
					<?php get_template_part('content', get_post_format()); ?>
					<!--/.Post-->
					<hr>
					<?php
	            } // end while
	            } // end if
	            ?>
							<?php mdb_pagination(); ?>
				</div>
				<!--Sidebar-->
				<div class="col-md-4">
					<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
					<?php dynamic_sidebar( 'sidebar' ); ?>
					<?php endif; ?>
				</div>
				<!--/.Sidebar-->
			</div>
		</div>
		<a id="icon" class="fa fa-arrow-up scrollToTop hidden-xs-down" style="text-shadow: none; font-size: 26px; color: rgb(255, 255, 255); height: 55px; width: 55px; line-height: 37px; border-radius: 50%; text-align: center; background-color: rgb(68, 167, 235);"></a>
	</div>
	<!--/.Main layout-->
	<?php get_footer(); ?>