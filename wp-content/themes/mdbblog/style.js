jQuery(document).ready(function ($) {

	//Check to see if the window is top if not then display button
	$(window).scroll(function () {
		if ($(this).scrollTop() > 500) {
			$('.scrollToTop').removeClass('animated bounceOutRight');
			$('.scrollToTop').addClass('animated bounceInRight');
			$('.scrollToTop').show();
		} else {
			$('.scrollToTop').removeClass('animated bounceInRight');
			$('.scrollToTop').addClass('animated bounceOutRight');
		}
	});

	//Click event to scroll to top
	$('.scrollToTop').click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 1500);
		return false;
	});

	// Navbar
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('.navbar').outerHeight();

	$(window).scroll(function (event) {
		didScroll = true;
	});

	setInterval(function () {
		if (didScroll) {
			hasScrolled();
			didScroll = false;
		}
	}, 250);

	function hasScrolled() {
		var st = $(this).scrollTop();

		// Make sure they scroll more than delta
		if (Math.abs(lastScrollTop - st) <= delta)
			return;

		// If they scrolled down and are past the navbar, add class .nav-up.
		// This is necessary so you never see what is "behind" the navbar.
		if (st > lastScrollTop && st > navbarHeight) {
			// Scroll Down
			$('.navbar').removeClass('animated fadeIn').addClass('animated fadeOut');
		} else {
			// Scroll Up
			if (st + $(window).height() < $(document).height()) {
				$('.navbar').removeClass('animated fadeOut').addClass('animated fadeIn');
			}
		}

		lastScrollTop = st;
	}
});